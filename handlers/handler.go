package handlers

import (
	"fmt"
	"net/http"
)

func sobe(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "["+
		"{\"id\":1, \"steviloOseb\": 3, \"balkon\":true},"+
		"{\"id\":2, \"steviloOseb\": 2, \"balkon\":false},"+
		"{\"id\":3, \"steviloOseb\": 2, \"balkon\":true},"+
		"{\"id\":4, \"steviloOseb\": 3, \"balkon\":false},"+
		"{\"id\":5, \"steviloOseb\": 1, \"balkon\":true},"+
		"{\"id\":6, \"steviloOseb\": 1, \"balkon\":true}]")
	fmt.Println("Endpoint Hit: homePage")
}

func rezervacije(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "["+
		"{\"id\":1, \"idSoba\": 1, \"status\":\"Rezervirana\", \"najemnik\":\"Rok\",\"datumOd\":\"9.12.2021\",\"datumDo\":\"15.12.2021\"},"+
		"{\"id\":2, \"idSoba\": 2, \"status\":\"Rezervirana\", \"najemnik\":\"Domen\",\"datumOd\":\"5.12.2021\",\"datumDo\":\"15.12.2021\"},"+
		"{\"id\":3, \"idSoba\": 3, \"status\":\"Rezervirana\", \"najemnik\":\"Amrjan\",\"datumOd\":\"3.12.2021\",\"datumDo\":\"15.12.2021\"},"+
		"{\"id\":4, \"idSoba\": 4, \"status\":\"Potrjena\", \"najemnik\":\"Maja\",\"datumOd\":\"2.12.2021\",\"datumDo\":\"15.12.2021\"},"+
		"{\"id\":5, \"idSoba\": 5, \"status\":\"Potrjena\", \"najemnik\":\"Tina\",\"datumOd\":\"6.12.2021\",\"datumDo\":\"15.12.2021\"},"+
		"{\"id\":6, \"idSoba\": 6, \"status\":\"V preklicu\", \"najemnik\":\"Patrik\",\"datumOd\":\"5.12.2021\",\"datumDo\":\"15.12.2021\"},"+
		"{\"id\":5, \"idSoba\": 6, \"status\":\"Preklicana\", \"najemnik\":\"Alen\",\"datumOd\":\"4.12.2021\",\"datumDo\":\"15.12.2021\"},"+
		"{\"id\":5, \"idSoba\": 5, \"status\":\"Preklicana\", \"najemnik\":\"Martina\",\"datumOd\":\"1.12.2021\",\"datumDo\":\"15.12.2021\"}]")
	fmt.Println("Endpoint Hit: Rezervacije")
}

func trenutni_gosti(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "[" +
	"{\"id\": 1, \"ime\": \"Maja\", \"priimek\": \"Kitek\", \"naslov\": \"izmišljena ulica 10, 404 neobstoječa pošta\", \"tel-st\": \"070123456\", \"idSoba\": 4}," + 
	"{\"id\": 2, \"ime\": \"Tina\", \"priimek\": \"Beranič\", \"naslov\": \"izmišljena ulica 28a, 404 neobstoječa pošta\", \"tel-st\": \"030123456\", \"idSoba\": 5}]")
	fmt.Println("Endpoint Hit: Trenutni_gosti")
}

func HandleRequests() http.Handler {
	mux := http.NewServeMux()
	mux.HandleFunc("/sobe", sobe)

	mux.HandleFunc("/rezervacije", rezervacije)

	mux.HandleFunc("/trenutni_gosti", trenutni_gosti)

	return mux
}
