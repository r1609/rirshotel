FROM alpine:latest
COPY Hotel_API /opt/Hotel_API

EXPOSE 8000

CMD ["/opt/Hotel_API"]
