package main

import (
	"Hotel_API/handlers"
	"log"
	"net/http"
)

func main() {

	log.Fatal(http.ListenAndServe(":8000", handlers.HandleRequests()))
}
