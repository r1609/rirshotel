package test

import (
	"Hotel_API/handlers"
	"github.com/gavv/httpexpect"
	"net/http/httptest"
	"testing"
)

func TestRezervacije(t *testing.T) {

	handler := handlers.HandleRequests()

	server := httptest.NewServer(handler)
	defer server.Close()

	e := httpexpect.New(t, server.URL)

	e.GET("/rezervacije").Expect().Body().Equal("[" +
		"{\"id\":1, \"idSoba\": 1, \"status\":\"Rezervirana\", \"najemnik\":\"Rok\",\"datumOd\":\"9.12.2021\",\"datumDo\":\"15.12.2021\"}," +
		"{\"id\":2, \"idSoba\": 2, \"status\":\"Rezervirana\", \"najemnik\":\"Domen\",\"datumOd\":\"5.12.2021\",\"datumDo\":\"15.12.2021\"}," +
		"{\"id\":3, \"idSoba\": 3, \"status\":\"Rezervirana\", \"najemnik\":\"Amrjan\",\"datumOd\":\"3.12.2021\",\"datumDo\":\"15.12.2021\"}," +
		"{\"id\":4, \"idSoba\": 4, \"status\":\"Potrjena\", \"najemnik\":\"Maja\",\"datumOd\":\"2.12.2021\",\"datumDo\":\"15.12.2021\"}," +
		"{\"id\":5, \"idSoba\": 5, \"status\":\"Potrjena\", \"najemnik\":\"Tina\",\"datumOd\":\"6.12.2021\",\"datumDo\":\"15.12.2021\"}," +
		"{\"id\":6, \"idSoba\": 6, \"status\":\"V preklicu\", \"najemnik\":\"Patrik\",\"datumOd\":\"5.12.2021\",\"datumDo\":\"15.12.2021\"}," +
		"{\"id\":5, \"idSoba\": 6, \"status\":\"Preklicana\", \"najemnik\":\"Alen\",\"datumOd\":\"4.12.2021\",\"datumDo\":\"15.12.2021\"}," +
		"{\"id\":5, \"idSoba\": 5, \"status\":\"Preklicana\", \"najemnik\":\"Martina\",\"datumOd\":\"1.12.2021\",\"datumDo\":\"15.12.2021\"}]")
}
