package test

import (
	"Hotel_API/handlers"
	"github.com/gavv/httpexpect/v2"
	"net/http/httptest"
	"testing"
)

func TestSobe(t *testing.T) {

	handler := handlers.HandleRequests()

	server := httptest.NewServer(handler)
	defer server.Close()

	e := httpexpect.New(t, server.URL)

	e.GET("/sobe").Expect().Body().Equal("[" +
		"{\"id\":1, \"steviloOseb\": 3, \"balkon\":true}," +
		"{\"id\":2, \"steviloOseb\": 2, \"balkon\":false}," +
		"{\"id\":3, \"steviloOseb\": 2, \"balkon\":true}," +
		"{\"id\":4, \"steviloOseb\": 3, \"balkon\":false}," +
		"{\"id\":5, \"steviloOseb\": 1, \"balkon\":true}," +
		"{\"id\":6, \"steviloOseb\": 1, \"balkon\":true}]")

}
