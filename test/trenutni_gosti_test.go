package test

import (
	"Hotel_API/handlers"
	"github.com/gavv/httpexpect"
	"net/http/httptest"
	"testing"
)

func TestTrenutni_gosti(t *testing.T) {

	handler := handlers.HandleRequests()

	server := httptest.NewServer(handler)
	defer server.Close()

	e := httpexpect.New(t, server.URL)

	e.GET("/trenutni_gosti").Expect().Body().Equal("[" +
		"{\"id\": 1, \"ime\": \"Maja\", \"priimek\": \"Kitek\", \"naslov\": \"izmišljena ulica 10, 404 neobstoječa pošta\", \"tel-st\": \"070123456\", \"idSoba\": 4}," +
		"{\"id\": 2, \"ime\": \"Tina\", \"priimek\": \"Beranič\", \"naslov\": \"izmišljena ulica 28a, 404 neobstoječa pošta\", \"tel-st\": \"030123456\", \"idSoba\": 5}]")
}
